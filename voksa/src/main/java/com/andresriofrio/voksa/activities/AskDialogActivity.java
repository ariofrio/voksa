package com.andresriofrio.voksa.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.andresriofrio.voksa.R;
import com.andresriofrio.voksa.util.CallUtils;

public class AskDialogActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Uri dataUri = getIntent().getData();
        if (dataUri == null) return;
        final String phoneNumberToCall = getIntent().getData().getSchemeSpecificPart();
        if (TextUtils.isEmpty(phoneNumberToCall)) return;

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(R.array.items_dialog_ask, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Context context = AskDialogActivity.this;
                if (which == 0) {
                    CallUtils.makeRoutedCall(context, phoneNumberToCall);
                } else if (which == 1) {
                    CallUtils.makeDirectCall(context, phoneNumberToCall);
                }
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                AskDialogActivity.this.finish();
            }
        });
        dialog.show();
    }


}
