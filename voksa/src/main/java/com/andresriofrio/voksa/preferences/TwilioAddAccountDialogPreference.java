package com.andresriofrio.voksa.preferences;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.andresriofrio.voksa.R;
import com.andresriofrio.voksa.util.Utils;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.instance.Account;

import java.util.Arrays;
import java.util.HashSet;

public class TwilioAddAccountDialogPreference extends DialogPreference {

    private static final String TAG = TwilioAddAccountDialogPreference.class.getSimpleName();
    private AsyncTask<Void, Void, Void> task;

    public TwilioAddAccountDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDialogLayoutResource(R.layout.dialog_add_account_twilio);
        setNegativeButtonText(R.string.cancel);
        setPositiveButtonText(R.string.login);
    }

    @Override
    protected void showDialog(Bundle state) {
        super.showDialog(state);
        assert getDialog() != null;

        final Button loginButton = ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE);
        assert loginButton != null;

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String accountSid = ((EditText) getDialog().findViewById(R.id.account_sid)).getText().toString();
                final String authToken = ((EditText) getDialog().findViewById(R.id.auth_token)).getText().toString();

                if (task != null) task.cancel(true);
                task = new AsyncTask<Void, Void, Void>() {
                    boolean dismiss = false;
                    String message = null;

                    @TargetApi(Build.VERSION_CODES.FROYO)
                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            TwilioRestClient client = new TwilioRestClient(accountSid, authToken);
                            try {
                                // Try to fetch the account status from the server.
                                final Account account = client.getAccount();
                                account.getStatus();

                                // If successful, create a new account in the preferences.
                                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                                final SharedPreferences.Editor editor = prefs.edit();
                                final HashSet<String> accounts = new HashSet<>(Arrays.asList(prefs.getString(getContext().getString(R.string.pref_key_accounts), "").split(",")));
                                boolean isNewAccount = accounts.add("twilio:" + accountSid);
                                if (!isNewAccount) {
                                    message = "There is already an account with this Account SID. Updating its Auth Token.";
                                    // TODO Allow the user to cancel updating the auth token.
                                }
                                editor.putString(getContext().getString(R.string.pref_key_accounts), TextUtils.join(",", accounts));
                                editor.commit();

                                // And store the credentials.
                                SharedPreferences accountPrefs = getContext().getSharedPreferences("twilio/" + accountSid, 0);
                                SharedPreferences.Editor accountEditor = accountPrefs.edit();
                                accountEditor.putString("twilio:account_sid", accountSid);
                                accountEditor.putString("twilio:auth_token", authToken);
                                accountEditor.putString("display_name", account.getFriendlyName());
                                accountEditor.commit();

                                // Finally, dismiss the dialog.
                                dismiss = true;
                            } catch(RuntimeException e) {
                                if (!(e.getCause() instanceof TwilioRestException)) throw e;

                                TwilioRestException ex = (TwilioRestException) e.getCause();
                                if (ex.getErrorCode() == 20003) { // Authenticate
                                    message = "Invalid Account SID or Auth Token. Please try again.";
                                } else {
                                    message = "Error Code " + ex.getErrorCode() + ". " + ex.getErrorMessage() + ". Please try again.";
                                    Log.e(TAG, message + " More info: " + ex.getMoreInfo());
                                }
                            }
                        } catch (IllegalArgumentException e) {
                            message = e.getMessage();
                        } catch (Throwable e) {
                            Log.wtf(TAG, e);
                            message = e.getMessage();
                        }
                        return null;
                    }

                    @Override
                    protected void onPreExecute() {
                        getDialog().findViewById(R.id.activity_bar).setVisibility(View.VISIBLE);
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        getDialog().findViewById(R.id.activity_bar).setVisibility(View.INVISIBLE);
                        if (message != null) {
                            Utils.showToast(getContext(), message);
                        }
                        if (dismiss) {
                            getDialog().dismiss();
                        }
                    }
                }.execute();
            }
        });
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == AlertDialog.BUTTON_NEGATIVE) {
            if (task != null) task.cancel(true);
        }
    }
}
