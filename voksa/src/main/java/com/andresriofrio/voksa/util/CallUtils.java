package com.andresriofrio.voksa.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.andresriofrio.voksa.R;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.parse.ParseException;
import com.parse.ParseObject;

public class CallUtils {
    private static final String TAG = CallUtils.class.getSimpleName();

    public static boolean shouldReroute(Context context, String phoneNumberToCall) {
        final String devicePhoneNumber = getDevicePhoneNumber(context);
        if (TextUtils.isEmpty(devicePhoneNumber)) return false;   // assume OFF
        final String virtualPhoneNumber = getVirtualPhoneNumber(context);
        if (TextUtils.isEmpty(virtualPhoneNumber)) return false; // assume OFF

        if (PhoneNumberUtils.compare(phoneNumberToCall, virtualPhoneNumber)) return false;

        // otherwise
        return true;
    }

    public static void makeRoutedCall(Context context, String phoneNumberToCall) {
        final String devicePhoneNumber = getDevicePhoneNumber(context);
        final String virtualPhoneNumber = getVirtualPhoneNumber(context);

        Log.i(TAG, "making a routed call to " + phoneNumberToCall + " through " + virtualPhoneNumber + " knowing this phone's number is " + devicePhoneNumber);

        // register new outgoing call request
        try {
            final ParseObject request = new ParseObject("OutgoingCallRequest");
            request.put("devicePhoneNumber", devicePhoneNumber);
            request.put("virtualPhoneNumber", virtualPhoneNumber);
            request.put("phoneNumberToCall", phoneNumberToCall);
            request.save();
        } catch (ParseException e) {
            Utils.showToast(context, "Voksa could not connect. Cancelling call.");

            // TODO: Retry with Voksa / Call without Voksa
//            final Intent retryIntent = new Intent(context, RetryDialogActivity.class);
//            retryIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(retryIntent);
            return;
        }

        // route call through virtual number
        Intent newIntent = new Intent("android.intent.action.CALL");
        newIntent.setData(Uri.parse("tel:" + Uri.encode(phoneNumberToCall)));
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        newIntent.putExtra("com.android.phone.extra.GATEWAY_PROVIDER_PACKAGE", context.getPackageName());
        newIntent.putExtra("com.android.phone.extra.GATEWAY_URI", "tel:" + Uri.encode(virtualPhoneNumber));
        context.startActivity(newIntent);
    }

    public static void makeDirectCall(Context context, String phoneNumberToCall) {
        Log.i(TAG, "making a direct call to " + phoneNumberToCall);

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString("do_not_reroute_phone_number", phoneNumberToCall);
        editor.commit();

        Intent newIntent = new Intent("android.intent.action.CALL");
        newIntent.setData(Uri.parse("tel:" + Uri.encode(phoneNumberToCall)));
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(newIntent);
    }

    public static String getVirtualPhoneNumber(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.pref_key_virtual_phone_number), null);
    }

    public static String getDevicePhoneNumber(Context context) {
        final String savedDevicePhoneNumber = PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.pref_key_device_phone_number), null);
        if (TextUtils.isEmpty(savedDevicePhoneNumber)) {
            return guessDevicePhoneNumber(context); // try to guess
        } else {
            return savedDevicePhoneNumber;
        }
    }

    public static boolean isAlreadyRerouted(Context context, Intent intent) {
        String virtualPhoneNumber = getVirtualPhoneNumber(context);
        if (TextUtils.isEmpty(virtualPhoneNumber)) return false;

        if (!intent.hasExtra("com.android.phone.extra.GATEWAY_URI")) return false;

        String gatewayPhoneNumber = Uri.parse(intent.getStringExtra("com.android.phone.extra.GATEWAY_URI")).getSchemeSpecificPart();
        if (!PhoneNumberUtils.compare(gatewayPhoneNumber, virtualPhoneNumber)) return false;

        // otherwise
        return true;
    }

    private static String guessDevicePhoneNumber(Context context) {
        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String rawPhoneNumber = tMgr.getLine1Number();
        try {
            return normalizePhoneNumber(rawPhoneNumber);
        } catch (NumberParseException e) {
            return null;
        }
    }

    private static String normalizePhoneNumber(String rawPhoneNumber) throws NumberParseException {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(rawPhoneNumber, null);
        return phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
    }
}
