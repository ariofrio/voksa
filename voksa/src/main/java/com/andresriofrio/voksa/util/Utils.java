package com.andresriofrio.voksa.util;

import android.content.Context;
import android.widget.Toast;

public class Utils {
    public static void showToast(Context context, String message) {
        final Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.show();
    }
}
