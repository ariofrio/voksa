package com.andresriofrio.voksa.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;

import com.andresriofrio.voksa.R;
import com.andresriofrio.voksa.activities.AskDialogActivity;
import com.andresriofrio.voksa.util.CallUtils;
import com.andresriofrio.voksa.util.Utils;

public class NewOutgoingCallReceiver extends BroadcastReceiver {
    private static final String TAG = NewOutgoingCallReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (CallUtils.isAlreadyRerouted(context, intent)) return;

        final String makingCalls = PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.pref_key_making_calls), "ASK");
        if (makingCalls.equals("OFF")) {
            Log.i(TAG, "making calls using a virtual number is OFF, ignoring");
            return;
        } else {
            final String phoneNumberToCall = getPhoneNumberToCall(intent);
            if (TextUtils.isEmpty(phoneNumberToCall)) {
                Log.i(TAG, "no phone number to call, ignoring");
                return;
            }
            final String doNotReroutePhoneNumber = PreferenceManager.getDefaultSharedPreferences(context).getString("do_not_reroute_phone_number", null);
            Log.d(TAG, "do not reroute phone number is " + doNotReroutePhoneNumber);
            if (doNotReroutePhoneNumber != null && PhoneNumberUtils.compare(doNotReroutePhoneNumber, phoneNumberToCall)) {
                final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
                editor.remove("do_not_reroute_phone_number");
                editor.commit();

                Log.i(TAG, "phone call was performed by us, ignoring");
                return;
            }
            final String devicePhoneNumber = CallUtils.getDevicePhoneNumber(context);
            if (TextUtils.isEmpty(devicePhoneNumber)) {
                Utils.showToast(context, "Cannot make call using Voksa. Please set your phone's number in Voksa Settings.");
                return;
            }
            final String virtualPhoneNumber = CallUtils.getVirtualPhoneNumber(context);
            if (TextUtils.isEmpty(virtualPhoneNumber)) {
                Utils.showToast(context, "Cannot make call using Voksa. Please set your virtual phone number in Voksa Settings.");
                return;
            }
            if(!CallUtils.shouldReroute(context, phoneNumberToCall)) {
                Log.i(TAG, "number to call is the virtual number, ignoring");
                return;
            }

            if (makingCalls.equals("ASK")) {
                Log.i(TAG, "starting ask dialog activity");
                final Intent askIntent = new Intent(context, AskDialogActivity.class);
                askIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                askIntent.setData(Uri.parse("tel:" + Uri.encode(phoneNumberToCall)));
                context.startActivity(askIntent);
                setResultData(null);
            } else if (makingCalls.equals("ON")) {
                Log.i(TAG, "making routed call");
                CallUtils.makeRoutedCall(context, phoneNumberToCall);
                setResultData(null);
            }
        }
    }

    private String getPhoneNumberToCall(Intent intent) {
        String phoneNumberToCall = getResultData();
        if (phoneNumberToCall == null) {
            phoneNumberToCall = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        }
        return phoneNumberToCall;
    }

}
